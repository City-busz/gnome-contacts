# Kazakh translation for gnome-contacts.
# Copyright (C) 2013 gnome-contacts's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-contacts package.
# Baurzhan Muftakhidinov <baurthefirst@gmail.com>, 2013-2022.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-contacts master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-contacts/issues\n"
"POT-Creation-Date: 2024-01-08 23:01+0000\n"
"PO-Revision-Date: 2024-02-24 22:55+0600\n"
"Last-Translator: Baurzhan Muftakhidinov <baurthefirst@gmail.com>\n"
"Language-Team: Kazakh <kk@li.org>\n"
"Language: kk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 3.4.2\n"

#: data/org.gnome.Contacts.appdata.xml.in.in:6
#: data/org.gnome.Contacts.desktop.in.in:3 data/ui/contacts-main-window.ui:125
#: src/contacts-main-window.vala:182 src/contacts-main-window.vala:205
#: src/main.vala:15
msgid "Contacts"
msgstr "Контакттар"

#: data/org.gnome.Contacts.appdata.xml.in.in:7
#: data/org.gnome.Contacts.desktop.in.in:4
msgid "Manage your contacts"
msgstr "Контакттарыңызды басқарыңыз"

#: data/org.gnome.Contacts.appdata.xml.in.in:9
msgid ""
"Contacts keeps and organize your contacts information. You can create, edit, "
"delete and link together pieces of information about your contacts. Contacts "
"aggregates the details from all your sources providing a centralized place "
"for managing your contacts."
msgstr ""
"Контакттар қолданбасы сіздің контакттар ақпаратын сақтайды және реттейді. "
"Сіз контакттарыңыз жөнінде ақпаратты жасап, түзетіп, өшіріп және өзара "
"сілтей аласыз. Контакттар қолданбасы барлық көздерден ақпаратты біріктіріп "
"ұстайды және контакттарды басқару үшін орталықтанған орны болып табылады."

#: data/org.gnome.Contacts.appdata.xml.in.in:15
msgid ""
"Contacts will also integrate with online address books and automatically "
"link contacts from different online sources."
msgstr ""
"Сонымен қатар, контакттар сіздің желідегі адрестік кітаптарды қолданады және "
"бірнеше желідегі көзден контакттарды автоматты түрде қолжетерлік қылады."

#: data/org.gnome.Contacts.appdata.xml.in.in:22
msgid "Contacts with no contacts"
msgstr "Деректері жоқ контакттар"

#: data/org.gnome.Contacts.appdata.xml.in.in:26
msgid "Contacts filled with contacts"
msgstr "Деректері бар контакттар"

#: data/org.gnome.Contacts.appdata.xml.in.in:30
msgid "Contacts in selection mode"
msgstr "Контакттар таңдау режимінде"

#: data/org.gnome.Contacts.appdata.xml.in.in:34
msgid "Contacts setup view"
msgstr "Контакттар баптау көрінісі"

#: data/org.gnome.Contacts.appdata.xml.in.in:38
msgid "Contacts edit view"
msgstr "Контакттар түзету көрінісі"

#. developer_name tag deprecated with Appstream 1.0
#: data/org.gnome.Contacts.appdata.xml.in.in:946 src/contacts-app.vala:143
msgid "The GNOME Project"
msgstr "GNOME жобасы"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Contacts.desktop.in.in:6
msgid "friends;address book;"
msgstr "friends;address book;достар;адрестік кітапша;"

#: data/gtk/help-overlay.ui:7
msgctxt "shortcut window"
msgid "Overview"
msgstr "Шолу"

#: data/gtk/help-overlay.ui:11
msgctxt "shortcut window"
msgid "Help"
msgstr "Көмек"

#: data/gtk/help-overlay.ui:17
msgctxt "shortcut window"
msgid "Open menu"
msgstr "Мәзірді ашу"

#: data/gtk/help-overlay.ui:23
msgctxt "shortcut window"
msgid "Show preferences"
msgstr "Баптауларды көрсету"

#: data/gtk/help-overlay.ui:29
msgctxt "shortcut window"
msgid "Create a new contact"
msgstr "Жаңа контакт жасау"

#: data/gtk/help-overlay.ui:35
msgctxt "shortcut window"
msgid "Search contacts"
msgstr "Контакттар ішінен іздеу"

#: data/gtk/help-overlay.ui:41
msgctxt "shortcut window"
msgid "Shortcut list"
msgstr "Жарлықтар тізімі"

#: data/gtk/help-overlay.ui:47
msgctxt "shortcut window"
msgid "Quit"
msgstr "Шығу"

#: data/gtk/help-overlay.ui:54
msgctxt "shortcut window"
msgid "Editing or creating a contact"
msgstr "Контактты түзету немесе жасау"

#: data/gtk/help-overlay.ui:58
msgctxt "shortcut window"
msgid "Save current changes to contact"
msgstr "Контакттың ағымдағы өзгерістерін сақтау"

#: data/gtk/help-overlay.ui:64
msgctxt "shortcut window"
msgid "Cancel current changes for contact"
msgstr "Контакттың ағымдағы өзгерістерін болдырмау"

#: data/ui/contacts-avatar-selector.ui:12
msgid "Select a new avatar"
msgstr "Жаңа аватарды таңдау"

#: data/ui/contacts-avatar-selector.ui:34 data/ui/contacts-crop-dialog.ui:34
#: data/ui/contacts-import-dialog.ui:36 data/ui/contacts-main-window.ui:288
#: src/contacts-app.vala:236 src/contacts-contact-editor.vala:708
#: src/contacts-main-window.vala:538
msgid "_Cancel"
msgstr "Ба_с тарту"

#: data/ui/contacts-avatar-selector.ui:44 data/ui/contacts-crop-dialog.ui:44
#: data/ui/contacts-setup-window.ui:30
msgid "_Done"
msgstr "_Дайын"

#: data/ui/contacts-avatar-selector.ui:90
msgid "Take a Picture…"
msgstr "Фотоны түсіру…"

#: data/ui/contacts-avatar-selector.ui:98
msgid "Select a File…"
msgstr "Файлды таңдау…"

#: data/ui/contacts-contact-pane.ui:19
msgid "Select a Contact"
msgstr "Контактты таңдау"

#: data/ui/contacts-editor-menu.ui:16
msgid "Change Addressbook"
msgstr "Адрестік кітапшаны ауыстыру"

#: data/ui/contacts-editable-avatar.ui:22
msgid "Change Avatar"
msgstr "Аватарды өзгерту"

#: data/ui/contacts-editable-avatar.ui:41
msgid "Remove Avatar"
msgstr "Аватарды өшіру"

#: data/ui/contacts-import-dialog.ui:12
msgid "Import Contacts"
msgstr "Контакттарды импорттау"

#: data/ui/contacts-import-dialog.ui:44
msgid "_Import"
msgstr "_Импорттау"

#: data/ui/contacts-link-suggestion-grid.ui:56
msgid "Link Contacts"
msgstr "Контакттарды байланыстыру"

#: data/ui/contacts-main-window.ui:10
msgid "List Contacts By:"
msgstr "Контакттарды шығару реті:"

#: data/ui/contacts-main-window.ui:12
msgid "First Name"
msgstr "Аты"

#: data/ui/contacts-main-window.ui:17
msgid "Surname"
msgstr "Фамилия"

#: data/ui/contacts-main-window.ui:24
msgid "Import From File…"
msgstr "Файлдан импорттау…"

#: data/ui/contacts-main-window.ui:28
msgid "Export All Contacts…"
msgstr "Барлық контакттарды экспорттау…"

#: data/ui/contacts-main-window.ui:34
msgid "Preferences"
msgstr "Баптаулар"

#: data/ui/contacts-main-window.ui:38
msgid "Keyboard Shortcuts"
msgstr "Пернетақта жарлықтары"

#: data/ui/contacts-main-window.ui:42
msgid "Help"
msgstr "Көмек"

#: data/ui/contacts-main-window.ui:46
msgid "About Contacts"
msgstr "Контакттар қолданбасы туралы"

#: data/ui/contacts-main-window.ui:55
msgid "Mark as Favorite"
msgstr "Таңдамалы ретінде белгілеу"

#: data/ui/contacts-main-window.ui:60
msgid "Unmark as Favorite"
msgstr "Таңдамалы емес ретінде белгілеу"

#: data/ui/contacts-main-window.ui:65
msgid "Share as QR Code"
msgstr "QR коды ретінде бөлісу"

#: data/ui/contacts-main-window.ui:71
msgid "Delete Contact"
msgstr "Контактты өшіру"

#: data/ui/contacts-main-window.ui:133
msgid "Add New Contact"
msgstr "Жаңа контакт қосу"

#: data/ui/contacts-main-window.ui:143
msgid "Main Menu"
msgstr "Негізгі мәзір"

#: data/ui/contacts-main-window.ui:150
msgid "Select Contacts"
msgstr "Контакттарды таңдау"

#: data/ui/contacts-main-window.ui:157 src/contacts-main-window.vala:439
msgid "Cancel"
msgstr "Бас тарту"

#: data/ui/contacts-main-window.ui:158
msgid "Cancel Selection"
msgstr "Таңдаудан бас тарту"

#: data/ui/contacts-main-window.ui:171
msgid "Search contacts"
msgstr "Контакттар ішінен іздеу"

#: data/ui/contacts-main-window.ui:197
msgid "Loading"
msgstr "Жүктеу"

#. Export refers to the verb
#: data/ui/contacts-main-window.ui:234
msgid "Export"
msgstr "Экспорттау"

#: data/ui/contacts-main-window.ui:235
msgid "Export Selected Contacts"
msgstr "Таңдалған контакттарды экспорттау"

#. Link refers to the verb, from linking contacts together
#: data/ui/contacts-main-window.ui:243
msgid "Link"
msgstr "Сілтеме"

#: data/ui/contacts-main-window.ui:244
msgid "Link Selected Contacts Together"
msgstr "Таңдалған контактілерді өзара байланыстыру"

#: data/ui/contacts-main-window.ui:259
msgid "Delete"
msgstr "Өшіру"

#: data/ui/contacts-main-window.ui:303
msgid "Edit Contact"
msgstr "Контактты түзету"

#: data/ui/contacts-main-window.ui:309
msgid "Contact Menu"
msgstr "Контакт мәзірі"

#: data/ui/contacts-main-window.ui:319 src/contacts-main-window.vala:214
msgid "Done"
msgstr "Дайын"

#: data/ui/contacts-preferences-window.ui:11
msgid "Address Books"
msgstr "Адрестік кітапшалар"

#: data/ui/contacts-qr-code-dialog.ui:14
msgid "Share Contact"
msgstr "Контактпен бөлісу"

#: data/ui/contacts-qr-code-dialog.ui:37
msgid "QR Code"
msgstr "QR коды"

#: data/ui/contacts-qr-code-dialog.ui:47
msgid "Scan to Save"
msgstr "Сақтау үшін сканерлеу"

#: data/ui/contacts-setup-window.ui:12
msgid "Contacts Setup"
msgstr "Контактты орнату"

#: data/ui/contacts-setup-window.ui:20
msgid "_Quit"
msgstr "_Шығу"

#: data/ui/contacts-setup-window.ui:22
msgid "Cancel Setup And Quit"
msgstr "Баптаудан бас тартып, шығу"

#. Translators: "Complete" is a verb here: a user finishes the setup by clicking this button
#: data/ui/contacts-setup-window.ui:32
msgid "Complete setup"
msgstr "Баптауды аяқтау"

#: data/ui/contacts-setup-window.ui:44
msgid "Welcome"
msgstr "Қош келдіңіз"

#: data/ui/contacts-setup-window.ui:45
msgid ""
"Please select your main address book: this is where new contacts will be "
"added. If you keep your contacts in an online account, you can add them "
"using the online accounts settings."
msgstr ""
"Біріншілік адрестік кітапшаны таңдаңыз: ол жерге жаңа контакттар қосылатын "
"болады. Егер сіз контакттарды желідегі тіркелгіде сақтасаңыз, онда ол "
"тіркелгіні желілік тіркелгілер баптауларында қоса аласыз."

#: src/contacts-app.vala:35
msgid "Show contact with this email address"
msgstr "Эл. поштасы осы болатын контактты көрсету"

#: src/contacts-app.vala:36
msgid "Show contact with this individual id"
msgstr "Жеке id осы болатын контактты көрсету"

#: src/contacts-app.vala:37
msgid "Show contacts with the given filter"
msgstr "Көрсетілген сүзгімен контактты көрсету"

#: src/contacts-app.vala:38
msgid "Show the current version of Contacts"
msgstr "Контакттар қолданбасының ағымдағы нұсқасын көрсетеді"

#: src/contacts-app.vala:104 src/contacts-app.vala:166
msgid "Contact not found"
msgstr "Контакт табылмады"

#: src/contacts-app.vala:105
#, c-format
msgid "No contact with id %s found"
msgstr "Идентификаторы %s болатын контакт табылмады"

#: src/contacts-app.vala:106 src/contacts-app.vala:168
#: src/contacts-avatar-selector.vala:162 src/contacts-avatar-selector.vala:251
msgid "_Close"
msgstr "_Жабу"

#: src/contacts-app.vala:149
msgid ""
"© 2011 Red Hat, Inc.\n"
"© 2011-2020 The Contacts Developers"
msgstr ""
"© 2011 Red Hat, Inc.\n"
"© 2011-2020 The Contacts әзірлеушілері"

#: src/contacts-app.vala:167
#, c-format
msgid "No contact with email address %s found"
msgstr "Эл. поштасы %s болатын контакт табылмады"

#: src/contacts-app.vala:230
msgid "Primary address book not found"
msgstr "Біріншілік адрестік кітапшасы табылмады"

#: src/contacts-app.vala:232
msgid ""
"Contacts can't find the configured primary address book. You might "
"experience issues creating or editing contacts"
msgstr ""
"Контакттар бапталған біріншілік адрестік кітапшаны таба алмады. Контакттарды "
"жасау немесе түзету кезінде мәселелер болуы мүмкін"

#: src/contacts-app.vala:233
msgid "Go To _Preferences"
msgstr "Ба_птауларға өту"

#: src/contacts-app.vala:334
msgid "Select contact file"
msgstr "Контакт файлын таңдау"

#: src/contacts-app.vala:335
msgid "Import"
msgstr "Импорттау"

#: src/contacts-app.vala:341
msgid "vCard files"
msgstr "vCard файлдары"

#: src/contacts-avatar-selector.vala:115
msgid "No Camera Detected"
msgstr "Камера табылмады"

#: src/contacts-avatar-selector.vala:161
msgid "Failed to set avatar"
msgstr "Аватарды орнату қатемен аяқталды"

#: src/contacts-avatar-selector.vala:198
msgid "Browse for more pictures"
msgstr "Көбірек суретті шолу"

#: src/contacts-avatar-selector.vala:199
msgid "_Open"
msgstr "_Ашу"

#: src/contacts-avatar-selector.vala:204
msgid "Image File"
msgstr "Сурет файлы"

#: src/contacts-avatar-selector.vala:250
msgid "Failed to set avatar."
msgstr "Аватарды орнату қатемен аяқталды."

#: src/contacts-contact-editor.vala:187
msgid "Show More"
msgstr "Көбірек көрсету"

#: src/contacts-contact-editor.vala:284
msgid "Add email"
msgstr "Эл. пошта адресін қосу"

#: src/contacts-contact-editor.vala:310
msgid "Add phone number"
msgstr "Телефон нөмірін қосу"

#: src/contacts-contact-editor.vala:336
msgid "Website"
msgstr "Веб сайт"

#: src/contacts-contact-editor.vala:347
#: src/core/contacts-full-name-chunk.vala:38
msgid "Full name"
msgstr "Толық аты"

#: src/contacts-contact-editor.vala:358
#: src/core/contacts-nickname-chunk.vala:36
msgid "Nickname"
msgstr "Ник аты"

#: src/contacts-contact-editor.vala:409
#: src/core/contacts-birthday-chunk.vala:40
msgid "Birthday"
msgstr "Туған күні"

#: src/contacts-contact-editor.vala:430
msgid "Remove birthday"
msgstr "Туған күнін өшіру"

#: src/contacts-contact-editor.vala:448
msgid "Set Birthday"
msgstr "Туған күнді орнату"

#: src/contacts-contact-editor.vala:502
msgid "Organization"
msgstr "Ұйым"

#: src/contacts-contact-editor.vala:508
msgid "Role"
msgstr "Рөлі"

#: src/contacts-contact-editor.vala:624
msgid "Label"
msgstr "Белгі"

#. Create grid and labels
#: src/contacts-contact-editor.vala:690
msgid "Day"
msgstr "Күн"

#: src/contacts-contact-editor.vala:694
msgid "Month"
msgstr "Ай"

#: src/contacts-contact-editor.vala:698
msgid "Year"
msgstr "Жыл"

#: src/contacts-contact-editor.vala:712
msgid "_Set"
msgstr "_Орнату"

#: src/contacts-contact-editor.vala:721
msgid "Change Birthday"
msgstr "Туған күнді өзгерту"

#: src/contacts-contact-editor.vala:775
msgid "Street"
msgstr "Көше"

#: src/contacts-contact-editor.vala:775
msgid "Extension"
msgstr "Кеңейту"

#: src/contacts-contact-editor.vala:775
msgid "City"
msgstr "Қала"

#: src/contacts-contact-editor.vala:775
msgid "State/Province"
msgstr "Аймақ"

#: src/contacts-contact-editor.vala:775
msgid "Zip/Postal Code"
msgstr "Индекс"

#: src/contacts-contact-editor.vala:775
msgid "PO box"
msgstr "Пошта жәшігі"

#: src/contacts-contact-editor.vala:775
msgid "Country"
msgstr "Ел"

#: src/contacts-contact-list.vala:136
msgid "Favorites"
msgstr "Таңдамалы"

#: src/contacts-contact-list.vala:138
msgid "All Contacts"
msgstr "Барлық контакттар"

#: src/contacts-contact-sheet.vala:204
#, c-format
msgid "Send an email to %s"
msgstr "%s адресіне эл. пошта хатын жіберу"

#: src/contacts-contact-sheet.vala:284
msgid "Visit website"
msgstr "Веб-сайтты шолу"

#: src/contacts-contact-sheet.vala:321
msgid "Their birthday is today! 🎉"
msgstr "Олардың туған күні бүгін! 🎉"

#: src/contacts-contact-sheet.vala:368
msgid "Show on the map"
msgstr "Картада көрсету"

#: src/contacts-delete-operation.vala:25
#, c-format
msgid "Deleting %d contact"
msgid_plural "Deleting %d contacts"
msgstr[0] "%d контактты өшіру"

#. Special-case the local address book
#: src/contacts-esd-setup.vala:140 src/contacts-utils.vala:108
msgid "Local Address Book"
msgstr "Жергілікті адрестік кітапшасы"

#: src/contacts-esd-setup.vala:143 src/contacts-esd-setup.vala:158
#: src/contacts-utils.vala:138
msgid "Google"
msgstr "Google"

#: src/contacts-esd-setup.vala:155
msgid "Local Contact"
msgstr "Жергілікті контакт"

#: src/contacts-im-service.vala:19
msgid "AOL Instant Messenger"
msgstr "AOL Instant Messenger"

#: src/contacts-im-service.vala:20
msgid "Facebook"
msgstr "Facebook"

#: src/contacts-im-service.vala:21
msgid "Gadu-Gadu"
msgstr "Gadu-Gadu"

#: src/contacts-im-service.vala:22
msgid "Google Talk"
msgstr "Google Talk"

#: src/contacts-im-service.vala:23
msgid "Novell Groupwise"
msgstr "Novell Groupwise"

#: src/contacts-im-service.vala:24
msgid "ICQ"
msgstr "ICQ"

#: src/contacts-im-service.vala:25
msgid "IRC"
msgstr "IRC"

#: src/contacts-im-service.vala:26
msgid "Jabber"
msgstr "Jabber"

#: src/contacts-im-service.vala:27
msgid "Livejournal"
msgstr "Livejournal"

#: src/contacts-im-service.vala:28
msgid "Local network"
msgstr "Жергілікті желі"

#: src/contacts-im-service.vala:29
msgid "Windows Live Messenger"
msgstr "Windows Live Messenger"

#: src/contacts-im-service.vala:30
msgid "MySpace"
msgstr "MySpace"

#: src/contacts-im-service.vala:31
msgid "MXit"
msgstr "MXit"

#: src/contacts-im-service.vala:32
msgid "Napster"
msgstr "Napster"

#: src/contacts-im-service.vala:33
msgid "Ovi Chat"
msgstr "Ovi Chat"

#: src/contacts-im-service.vala:34
msgid "Tencent QQ"
msgstr "Tencent QQ"

#: src/contacts-im-service.vala:35
msgid "IBM Lotus Sametime"
msgstr "IBM Lotus Sametime"

#: src/contacts-im-service.vala:36
msgid "SILC"
msgstr "SILC"

#: src/contacts-im-service.vala:37
msgid "sip"
msgstr "sip"

#: src/contacts-im-service.vala:38
msgid "Skype"
msgstr "Skype"

#: src/contacts-im-service.vala:39
msgid "Telephony"
msgstr "Telephony"

#: src/contacts-im-service.vala:40
msgid "Trepia"
msgstr "Trepia"

#: src/contacts-im-service.vala:41 src/contacts-im-service.vala:42
msgid "Yahoo! Messenger"
msgstr "Yahoo! Messenger"

#: src/contacts-im-service.vala:43
msgid "Zephyr"
msgstr "Zephyr"

#: src/contacts-import-dialog.vala:60
msgid "An error occurred reading the selected file"
msgstr "Таңдалған файлды оқу кезінде қате орын алды"

#: src/contacts-import-dialog.vala:87
#, c-format
msgid "An error occurred reading the file '%s'"
msgstr "\"%s\" файлын оқу кезінде қате орын алды"

#: src/contacts-import-dialog.vala:95
msgid "The imported file does not seem to contain any contacts"
msgstr "Импортталған файлда ешқандай контакттар жоқ сияқты"

#: src/contacts-import-dialog.vala:100
#, c-format
msgid "Found %u contact"
msgid_plural "Found %u contacts"
msgstr[0] "%u контакт табылды"

#: src/contacts-import-dialog.vala:129
msgid "Can't import: no contacts found"
msgstr "Импорттау мүмкін емес: контакттар табылмады"

#: src/contacts-import-dialog.vala:134
#, c-format
msgid "By continuing, you will import %u contact"
msgid_plural "By continuing, you will import %u contacts"
msgstr[0] "Жалғастыра отырып, %u контактіні импорттайсыз"

#: src/contacts-link-operation.vala:27
#, c-format
msgid "Linked %d contact"
msgid_plural "Linked %d contacts"
msgstr[0] "%d контакт байланыстырылды"

#: src/contacts-link-suggestion-grid.vala:38
#, c-format
msgid "Is this the same person as %s from %s?"
msgstr "Бұл контакт %s контактымен (%s ішінен) бірдей ме?"

#: src/contacts-link-suggestion-grid.vala:41
#, c-format
msgid "Is this the same person as %s?"
msgstr "Бұл контакт %s контактымен бірдей ме?"

#: src/contacts-main-window.vala:184
#, c-format
msgid "%llu Selected"
msgid_plural "%llu Selected"
msgstr[0] "%llu таңдалған"

#: src/contacts-main-window.vala:214
msgid "_Add"
msgstr "Қ_осу"

#: src/contacts-main-window.vala:242
#, c-format
msgid "Editing %s"
msgstr "%s түзету"

#: src/contacts-main-window.vala:281
#, c-format
msgid "%d Selected"
msgid_plural "%d Selected"
msgstr[0] "%d таңдалды"

#: src/contacts-main-window.vala:301 src/contacts-main-window.vala:509
#: src/contacts-main-window.vala:551
msgid "_Undo"
msgstr "Бол_дырмау"

#: src/contacts-main-window.vala:383
msgid "New Contact"
msgstr "Жаңа контакт"

#: src/contacts-main-window.vala:437
msgid "Discard changes?"
msgstr "Өзгерістерді елемеу керек пе?"

#: src/contacts-main-window.vala:438
msgid "Changes which are not saved will be permanently lost."
msgstr "Сақталмаған өзгерістер толығымен жоғалады."

#: src/contacts-main-window.vala:440
msgid "Discard"
msgstr "Елемеу"

#. Open up a file chooser
#: src/contacts-main-window.vala:592
msgid "Export to file"
msgstr "Файлға экспорттау"

#: src/contacts-main-window.vala:593
msgid "_Export"
msgstr "_Экспорттау"

#: src/contacts-main-window.vala:594
msgid "contacts.vcf"
msgstr "contacts.vcf"

#: src/contacts-preferences-window.vala:19
msgid "Primary Address Book"
msgstr "Біріншілік адрестік кітапшасы"

#: src/contacts-preferences-window.vala:20
msgid ""
"New contacts will be added to the selected address book. You are able to "
"view and edit contacts from other address books."
msgstr ""
"Жаңа контакттар таңдалған адрестік кітапшасына қосылады. Сіз басқа адрестік "
"кітапшалар контакттарын көріп, түзете аласыз."

#: src/contacts-preferences-window.vala:29
msgid "_Online Accounts"
msgstr "_Желілік тіркелгілер"

#: src/contacts-preferences-window.vala:34
msgid "Open the Online Accounts panel in Settings"
msgstr "Баптауларда Желілік тіркелгілер панелін ашу"

#: src/contacts-qr-code-dialog.vala:22
#, c-format
msgid "Scan the QR code to save the contact <b>%s</b>."
msgstr "<b>%s</b> контактын сақтау үшін QR кодын сканерлеңіз."

#: src/contacts-unlink-operation.vala:26
msgid "Unlinking contacts"
msgstr "Контакттарды ажырату"

#: src/core/contacts-addresses-chunk.vala:17
msgid "Postal addresses"
msgstr "Пошта адрестері"

#: src/core/contacts-alias-chunk.vala:33
msgid "Alias"
msgstr "Алиас"

#: src/core/contacts-avatar-chunk.vala:27
msgid "Avatar"
msgstr "Аватар"

#: src/core/contacts-contact.vala:27
msgid "Unnamed Person"
msgstr "Аты жоқ адам"

#: src/core/contacts-email-addresses-chunk.vala:13
msgid "Email addresses"
msgstr "Эл. пошта адрестері"

#: src/core/contacts-im-addresses-chunk.vala:18
msgid "Instant Messaging addresses"
msgstr "Жылдам хабар алмасу адрестері"

#: src/core/contacts-notes-chunk.vala:17
msgid "Notes"
msgstr "Естеліктер"

#: src/core/contacts-phones-chunk.vala:17
msgid "Phone numbers"
msgstr "Телефон нөмірлері"

#. TRANSLATORS: This is the role of a contact in an organisation (e.g. CEO)
#: src/core/contacts-roles-chunk.vala:19
msgid "Roles"
msgstr "Рөлдер"

#. TRANSLATORS: "$ROLE at $ORGANISATION", e.g. "CEO at Linux Inc."
#: src/core/contacts-roles-chunk.vala:118
#, c-format
msgid "%s at %s"
msgstr "%s, %s"

#. TRANSLATORS: This is a field which contains a name decomposed in several
#. parts, rather than a single freeform string for the full name
#: src/core/contacts-structured-name-chunk.vala:41
msgid "Structured name"
msgstr "Құрылымдалған аты"

#: src/core/contacts-type-descriptor.vala:71
#: src/core/contacts-type-set.vala:221
msgid "Other"
msgstr "Басқа"

#. List most specific first, always in upper case
#: src/core/contacts-type-set.vala:169 src/core/contacts-type-set.vala:190
#: src/core/contacts-type-set.vala:216 src/core/contacts-type-set.vala:218
msgid "Home"
msgstr "Үй"

#: src/core/contacts-type-set.vala:170 src/core/contacts-type-set.vala:191
#: src/core/contacts-type-set.vala:210 src/core/contacts-type-set.vala:212
msgid "Work"
msgstr "Жұмыс"

#. List most specific first, always in upper case
#: src/core/contacts-type-set.vala:189
msgid "Personal"
msgstr "Жеке"

#. List most specific first, always in upper case
#: src/core/contacts-type-set.vala:209
msgid "Assistant"
msgstr "Көмекші"

#: src/core/contacts-type-set.vala:211
msgid "Work Fax"
msgstr "Жұмыс факсы"

#: src/core/contacts-type-set.vala:213
msgid "Callback"
msgstr "Кері хабарлау"

#: src/core/contacts-type-set.vala:214
msgid "Car"
msgstr "Авто"

#: src/core/contacts-type-set.vala:215
msgid "Company"
msgstr "Компания"

#: src/core/contacts-type-set.vala:217
msgid "Home Fax"
msgstr "Үй факсы"

#: src/core/contacts-type-set.vala:219
msgid "ISDN"
msgstr "ISDN"

#: src/core/contacts-type-set.vala:220
msgid "Mobile"
msgstr "Мобильді"

#: src/core/contacts-type-set.vala:222
msgid "Fax"
msgstr "Факс"

#: src/core/contacts-type-set.vala:223
msgid "Pager"
msgstr "Пейджер"

#: src/core/contacts-type-set.vala:224
msgid "Radio"
msgstr "Радио"

#: src/core/contacts-type-set.vala:225
msgid "Telex"
msgstr "Телекс"

#. To translators: TTY is Teletypewriter
#: src/core/contacts-type-set.vala:227
msgid "TTY"
msgstr "TTY"

#: src/core/contacts-urls-chunk.vala:17
msgid "URLs"
msgstr "URL адрестері"

#: src/io/contacts-io-parse-operation.vala:29
#, c-format
msgid "Importing contacts from '%s'"
msgstr "\"%s\" ішінен контакттарды импорттау"

#: src/org.gnome.Contacts.gschema.xml:6
msgid "First-time setup done."
msgstr "Алғашқы баптаулар аяқталды."

#: src/org.gnome.Contacts.gschema.xml:7
msgid "Set to true after the user has run the first-time setup wizard."
msgstr ""
"Пайдаланушы алғашқы баптаулар шеберін жөнелткеннен кейін осыны true етіп "
"орнату."

#: src/org.gnome.Contacts.gschema.xml:11
msgid "Sort contacts on surname."
msgstr "Контакттарды фамилиясы бойынша сұрыптау."

#: src/org.gnome.Contacts.gschema.xml:12
msgid ""
"If this is set to true, the list of contacts will be sorted on their "
"surnames. Otherwise, it will be sorted on the first names of the contacts."
msgstr ""
"Бұның мәні ақиқат болса, контакттар тізімі фамилия бойынша сұрыпталады. "
"Болмаса, контакттар аттары бойынша сұрыпталады."

#: src/org.gnome.Contacts.gschema.xml:19
msgid "The default height of the contacts window."
msgstr "Контакттар тізімінің үнсіз келісім бойынша биіктігі."

#: src/org.gnome.Contacts.gschema.xml:20
msgid ""
"If the window size has not been changed by the user yet this will be used as "
"the initial value for the height of the window."
msgstr ""
"Терезе өлшемі пайдаланушымен әлі өзгертілмеген болса, онда бұл шама "
"терезенің бастапқы биіктігі ретінде қолданалатын болады."

#: src/org.gnome.Contacts.gschema.xml:26
msgid "The default width of the contacts window."
msgstr "Контакттар тізімінің үнсіз келісім бойынша ені."

#: src/org.gnome.Contacts.gschema.xml:27
msgid ""
"If the window size has not been changed by the user yet this will be used as "
"the initial value for the width of the window."
msgstr ""
"Терезе өлшемі пайдаланушымен әлі өзгертілмеген болса, онда бұл шама "
"терезенің бастапқы ені ретінде қолданалатын болады."

#: src/org.gnome.Contacts.gschema.xml:32
msgid "Is the window maximized?"
msgstr "Терезе максималды етілген бе?"

#: src/org.gnome.Contacts.gschema.xml:33 src/org.gnome.Contacts.gschema.xml:38
msgid "Stores if the window is currently maximized."
msgstr "Терезе максималды етілген бе, соны сақтайды."

#: src/org.gnome.Contacts.gschema.xml:37
msgid "Is the window fullscreen"
msgstr "Терезе максималды етілген бе"

#~ msgid "Remove"
#~ msgstr "Өшіру"

#~ msgid "Error reading file"
#~ msgstr "Файлды оқу қатесі"

#~ msgid "_OK"
#~ msgstr "_ОК"

#~ msgid "Continue Import?"
#~ msgstr "Импорттауды жалғастыру керек пе?"

#~ msgid "C_ontinue"
#~ msgstr "Жа_лғастыру"

#~ msgid "A contacts manager for GNOME"
#~ msgstr "GNOME үшін контакттарды басқарушысы"

#~ msgctxt "shortcut window"
#~ msgid "Search"
#~ msgstr "Іздеу"

#~ msgid "Linked Accounts"
#~ msgstr "Байланысқан тіркелгілер"

#~ msgid "You can link contacts by selecting them from the contacts list"
#~ msgstr ""
#~ "Контакттар тізімінен контакттарды таңдау арқылы оларды байланыстыра аласыз"

#~ msgid "Create new contact"
#~ msgstr "Жаңа контакт жасау"

#~ msgid "Menu"
#~ msgstr "Мәзір"

#~ msgid "Select Items"
#~ msgstr "Нәрселерді таңдау"

#~ msgid "Type to search"
#~ msgstr "Іздеу үшін теріңіз"

#~ msgid "Back"
#~ msgstr "Артқа"

#~ msgid "Add name"
#~ msgstr "Атын қосу"

#~ msgid "Set"
#~ msgstr "Орнату"

#~ msgid "Unlink"
#~ msgstr "Сілтеуді өшіру"

#~ msgid "Calendar event"
#~ msgstr "Күнтізбе оқиғасы"

#~ msgid "Gender"
#~ msgstr "Жынысы"

#~ msgid "Group"
#~ msgstr "Топ"

#~ msgid "Favourite"
#~ msgstr "Таңдамалы қылу"

#~ msgid "Local ID"
#~ msgstr "Жергілікті идентификатор"

#~ msgid "Note"
#~ msgstr "Естелік"

#~ msgid "Address"
#~ msgstr "Адрес"

#~ msgid "Web service"
#~ msgstr "Веб-қызметі"

#~ msgid "Change Address Book…"
#~ msgstr "Адрестік кітапшасын ауыстыру…"

#~ msgid "Online Accounts <sup>↗</sup>"
#~ msgstr "Желілік тіркелгілер <sup>↗</sup>"

#~ msgid "Change Address Book"
#~ msgstr "Адрестік кітапшасын ауыстыру"

#~ msgid "Change"
#~ msgstr "Өзгерту"

#~ msgid ""
#~ "New contacts will be added to the selected address book.\n"
#~ "You are able to view and edit contacts from other address books."
#~ msgstr ""
#~ "Жаңа контакттар таңдалған адрестік кітапшасына қосылады.\n"
#~ "Сіз басқа адрестік кітапшалар контакттарын көріп, түзете аласыз."

#~ msgid "translator-credits"
#~ msgstr "Baurzhan Muftakhidinov <baurthefirst@gmail.com>"

#~ msgid "About GNOME Contacts"
#~ msgstr "GNOME контакттары туралы"

#~ msgid "Contact Management Application"
#~ msgstr "Контакттарды басқару қолданбасы"

#, c-format
#~ msgid "Unable to create new contacts: %s"
#~ msgstr "Жаңа контакттарды жасау мүмкін емес: %s"

#~ msgid "Unable to find newly created contact"
#~ msgstr "Жаңадан жасалған контактты табу мүмкін емес"

#~ msgid "Start a call"
#~ msgstr "Қоңырауды бастау"

#~ msgid "Delete field"
#~ msgstr "Өрісті өшіру"

#~ msgid "https://example.com"
#~ msgstr "https://example.com"

#~ msgid "Take Another…"
#~ msgstr "Басқа суретті түсіру…"

#~ msgid "Share"
#~ msgstr "Бөлісу"

#~ msgid "Edit"
#~ msgstr "Түзету"

#~ msgid "Add contact"
#~ msgstr "Контактты қосу"

#~ msgid "Setup complete"
#~ msgstr "Баптау сәтті"

#~ msgid "GNOME Contacts"
#~ msgstr "GNOME контакттары"

#~ msgid "Unable to take photo."
#~ msgstr "Фотоны түсіру мүмкін емес."

#~ msgid "Add number"
#~ msgstr "Нөмірді қосу"

#~ msgid "%d contacts linked"
#~ msgid_plural "%d contacts linked"
#~ msgstr[0] "%d контакт байланыстырылған"

#~ msgid "%d contact deleted"
#~ msgid_plural "%d contacts deleted"
#~ msgstr[0] "%d контакт өшірілген"

#~ msgid "%s linked to %s"
#~ msgstr "%s байланысқан: %s"

#~ msgid "Google Circles"
#~ msgstr "Google Circles"

#~ msgid "Add"
#~ msgstr "Қосу"

#~ msgid "org.gnome.Contacts"
#~ msgstr "org.gnome.Contacts"

#~ msgid "Home email"
#~ msgstr "Үй эл. поштасы"

#~ msgid "Work email"
#~ msgstr "Жұмыс эл. поштасы"

#~ msgid "Mobile phone"
#~ msgstr "Мобильді телефоны"

#~ msgid "Home phone"
#~ msgstr "Үй телефон нөмірі"

#~ msgid "Work phone"
#~ msgstr "Жұмыс телефон нөмірі"

#~ msgid "Work address"
#~ msgstr "Жұмыс адресі"

#~ msgid "New Detail"
#~ msgstr "Жаңа ақпараты"

#~ msgid "Quit"
#~ msgstr "Шығу"

#~ msgid "Edit details"
#~ msgstr "Ақпаратын түзету"

#~ msgid "You need to enter some data"
#~ msgstr "Мәліметті енгізу керек"

#~ msgid "Unexpected internal error: created contact was not found"
#~ msgstr "Күтпеген ішкі қате: жасалған контакт табылмады"

#~ msgid "_Help"
#~ msgstr "_Көмек"

#~ msgid "_About"
#~ msgstr "Осы тур_алы"

#~ msgid "Selection mode"
#~ msgstr "Таңдау режимі"

#~ msgid "Select Address Book"
#~ msgstr "Адрестік кітапшасын таңдаңыз"

#~ msgid "Install GNOME Maps to open location."
#~ msgstr "Орналасуларды ашу үшін GNOME Maps қолданбасын орнатыңыз."

#~ msgid "— contact management"
#~ msgstr "— контакттарды басқару"

#~ msgid "Does %s from %s belong here?"
#~ msgstr "%s, %s ішінен осында тиісті ме?"

#~ msgid "Do these details belong to %s?"
#~ msgstr "Бұл ақпарат %s үшін тиісті ме?"

#~ msgid "Yes"
#~ msgstr "Иә"

#~ msgid "No"
#~ msgstr "Жоқ"

#~ msgid "%s"
#~ msgstr "%s"

#~ msgid "No results matched search"
#~ msgstr "Іздеу үшін сәйкестіктер жоқ"

#~ msgid "Suggestions"
#~ msgstr "Ұсыныстар"

#~ msgid "Contact deleted: “%s”"
#~ msgstr "Контакт өшірілді: \"%s\""

#~ msgid "View subset"
#~ msgstr "Ішкі жиынын қарау"

#~| msgid "friends;address book;"
#~ msgid "x-office-address-book"
#~ msgstr "x-office-address-book"

#~ msgid "January"
#~ msgstr "Қаңтар"

#~ msgid "February"
#~ msgstr "Ақпан"

#~ msgid "April"
#~ msgstr "Сәуір"

#~ msgid "June"
#~ msgstr "Маусым"

#~ msgid "July"
#~ msgstr "Шілде"

#~ msgid "August"
#~ msgstr "Тамыз"

#~ msgid "September"
#~ msgstr "Қыркүйек"

#~ msgid "October"
#~ msgstr "Қазан"

#~ msgid "November"
#~ msgstr "Қараша"

#~ msgid "December"
#~ msgstr "Желтоқсан"

#~ msgid "_Change Address Book..."
#~ msgstr "Адрестік кітапшасын ауы_стыру..."

#~ msgid "Personal email"
#~ msgstr "Жеке эл. пошта"

#~ msgid "Primary Contacts Account"
#~ msgstr "Контакттардың негізгі тіркелгісі"

#~ msgid "Twitter"
#~ msgstr "Twitter"

#~ msgid "Google Profile"
#~ msgstr "Google Profile"

#~ msgid "Google Other Contact"
#~ msgstr "Google басқа контакты"

#~ msgid "%s - Linked Accounts"
#~ msgstr "%s - Байланысқан тіркелгілер"

#~ msgid ""
#~ "Add or \n"
#~ "select a picture"
#~ msgstr ""
#~ "Суретті қосу \n"
#~ "не таңдау"

#~ msgid "Contact Name"
#~ msgstr "Контакт аты"

#~ msgid "Email"
#~ msgstr "Эл. пошта"

#~ msgid "Phone"
#~ msgstr "Телефон"

#~ msgid "You must specify a contact name"
#~ msgstr "Контакт атын көрсету керек"

#~ msgid "Please select your primary contacts account"
#~ msgstr "Біріншілік контакттар тіркелгісін таңдаңыз"
